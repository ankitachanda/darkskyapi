package com.idfbins.achanda;

import tk.plogitech.darksky.forecast.*;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

import java.time.Instant;
import java.time.temporal.ChronoUnit;


public class Sandbox {

    public static void main(String[] args) throws ForecastException {
        System.out.println("Hello, world!");

        ForecastRequest request = new ForecastRequestBuilder().key(new APIKey("4a7cc147626e51ade6c03736b8b99c91")).time(Instant.now().minus(5,ChronoUnit.DAYS))
                .language(ForecastRequestBuilder.Language.de).units(ForecastRequestBuilder.Units.us).exclude(ForecastRequestBuilder.Block.minutely).extendHourly()
                .location(new GeoCoordinates(new Longitude(-112.380093), new Latitude(42.770177))).build();

        DarkSkyClient client = new DarkSkyClient();
        String forecast = client.forecastJsonString(request);
        System.out.println("Forecast " + forecast);






    }




}
